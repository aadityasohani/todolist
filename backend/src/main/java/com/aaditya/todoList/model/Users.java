package com.aaditya.todoList.model;


import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Users {
    @Id
    private String loginid;
    private String username;
    private String password;

    public Users(String loginid, String username, String password, String authkey) {
        this.loginid = loginid;
        this.username = username;
        this.password = password;
    }

    public Users() {
    }

    public String getLoginid() {
        return loginid;
    }

    public void setLoginid(String loginid) {
        this.loginid = loginid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
