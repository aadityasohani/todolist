package com.aaditya.todoList.service;


import com.aaditya.todoList.model.Users;
import com.aaditya.todoList.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;
    public String addUser(String username,String password){
        int x = (int)userRepository.count();
        Users user = new Users();
        user.setUsername(username);
        user.setPassword(password);
        user.setLoginid(username+(x+1));

        userRepository.save(user);
        return "user added successfully";
    }


    public List<String> users(){
        return userRepository.getAllUsernames();
    }
}
