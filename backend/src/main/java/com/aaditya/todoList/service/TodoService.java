package com.aaditya.todoList.service;


import com.aaditya.todoList.model.Todos;
import com.aaditya.todoList.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TodoService {
    @Autowired
    TodoRepository todoRepository;


    public List<Todos> getTodos(String loginid){
        return todoRepository.findAllByLoginid(loginid);
    }

    public Todos addTodos(String loginid, String todo){
        Todos todos = new Todos();
        todos.setTodo(todo);
        int todoCount = (int)todoRepository.count()+1;
        todos.setTodoid(todoCount);
        todos.setLoginid(loginid);

        todoRepository.save(todos);
        return todos;
    }

    public String deleteTodo(int id){
        todoRepository.deleteByTodoid(id);
        return "deleted successfully";
    }

    public String changeCompletionStatus(int id){
        Todos todos = todoRepository.getById(id);
        todos.setCompleted(!todos.isCompleted());
        todoRepository.save(todos);
        return "updated successfully";
    }

    public String editTodo(int id,String todo){
        Todos todos = todoRepository.getById(id);
        todos.setTodo(todo);
        todoRepository.save(todos);

        return "updated successfully";
    }
}
