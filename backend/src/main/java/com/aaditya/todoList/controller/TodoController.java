package com.aaditya.todoList.controller;

import com.aaditya.todoList.model.Todos;
import com.aaditya.todoList.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TodoController {
    @Autowired
    TodoService todoService;


    @GetMapping("/getTodos/{loginid}")
    public List<Todos> getTodos(@PathVariable String loginid){
        return todoService.getTodos(loginid);
    }

    @PostMapping("/addTodo/{loginid}")
    public Todos addTodos(@PathVariable String loginid, @RequestBody String todo){
        return todoService.addTodos(loginid,todo);
    }

    @Transactional
    @DeleteMapping("/deleteTodo/{todoid}")
    public String delTodos(@PathVariable int todoid){
        return todoService.deleteTodo(todoid);
    }


    @Transactional
    @PutMapping("/editTodo/{todoid}")
    public String editTodo(@PathVariable int todoid,@RequestBody String todo){
        return todoService.editTodo(todoid,todo);
    }


    @Transactional
    @PutMapping("/changeStatus/{todoid}")
    public String changeStatus(@PathVariable int todoid){
        return todoService.changeCompletionStatus(todoid);
    }



}
