package com.aaditya.todoList.controller;
import com.aaditya.todoList.model.Users;
import com.aaditya.todoList.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {
    @Autowired
    UserService userService;

    @PostMapping("/newUser")
    public String addNewUser(@RequestBody Users user){
        return userService.addUser(user.getUsername(),user.getPassword());
    }

    @GetMapping("/users")
    public List<String> getAllUsers(){
        return userService.users();
    }
}
