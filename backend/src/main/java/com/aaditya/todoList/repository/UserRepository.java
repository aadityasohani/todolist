package com.aaditya.todoList.repository;

import com.aaditya.todoList.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<Users, String> {
    @Query(value = "SELECT username FROM users.USERS",nativeQuery = true)
    List<String> getAllUsernames();
}
