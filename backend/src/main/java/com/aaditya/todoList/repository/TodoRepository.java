package com.aaditya.todoList.repository;

import com.aaditya.todoList.model.Todos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TodoRepository extends JpaRepository<Todos,Integer> {
    List<Todos> findAllByLoginid(String id);
    void deleteByTodoid(int id);
}
